# RESTful Lens Examples

This repo contains examples of files used in the RESTful Lens. Each example will contain a sample input source file, the mapping file, as well as the output data and output provenance data.

### [Example 1](https://data-lens.atlassian.net/wiki/spaces/DL/pages/245858305/Quick+Start+Guide+-+RESTful+Lens+v1.1#2.-Creating-the-JSON-Configuration-File)

This basic example for the Quick Guide purpose is design to take the some details of articles and related with them entities from remote RESTful endpoint. The data endpoint is emulated by static files in `restful-lens/example1/input/restful`. The collected fields (`includeFields` parameter) are listed in JSON configuration file at `restful-lens/example1/input/multipage-config.json` file.

### [Example 2](https://data-lens.atlassian.net/wiki/spaces/DL/pages/245858305/Quick+Start+Guide+-+RESTful+Lens+v1.1#2.-Creating-the-JSON-Configuration-File)

The same basic example as in Example 1 but with switched on provenance and local Kafka server. Together with the same output you can see the generated provenance files in `restful-lens/example2/prov-output`. In this example JSON config file is read from remote location on Bitbucket (setting: `JSON_API_CONFIG_URL=https://bitbucket.org/data-lens/datalens-examples/raw/master/restful-lens/example1/input/multipage-config.json`).

