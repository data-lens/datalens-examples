
# SQL Lens Examples

This repo contains examples of files used in the SQL Lens. Each example will contain a mapping file and the output data. The database details will be explained for each example.

> All mapping examples will have their credentials removed, you must replace these with your own credentials.

### [Example 1](https://data-lens.atlassian.net/wiki/spaces/DL/pages/267059223/Manually+Create+a+Mapping+File#SQL)

This basic SQL example is most similar to the CSV example as the two share a similar data structure. We are just using the id, firstname and lastname to create our subject predicate objects, but this example’s main purpose is for targeting a database and its tables. The source DB is referenced in the rml:source as <#DB_source>, which can then seen at the end of the file. Here the table name is specified, this will then return all records and values from this table.

    Both examples 1 and 2 use the following database:

|  id   | firstname | lastname | occupation |
|  --   |     --    |    --    |     --     |
| 10001 |   Alice   | Johnson  |    Tech    |
| 10002 |    Bob    |  Smith   |    Sales   |


### [Example 2](https://data-lens.atlassian.net/wiki/spaces/DL/pages/267059223/Manually+Create+a+Mapping+File#SQL-with-Custom-Query)

This SQL example builds on the previous one, where here we also include a custom SQL query. Note that we are using a MySQL DB, so the syntax may differ. Setting up the rml:source and rr:sqlVersion is the same as before, however now we are dropping the rr:tableName in place of rml:query and rml:referenceFormulation ql:CSV. As well as the id, firstname and lastname subject predicate objects, we are also taking the data from the newly formulated namelength column. This is done in the exact same way as before, by setting a predicate, in this case ex:namelength, then defining the object, a reference to the value as a literal int. 

### Example 3

