# Stream Lens Examples

This repo contains examples of files used in the Stream File Lens. Each example will contain a sample input source file, the mapping file, as well as the output data and output provenance data.

### Example 1

This basic CSV example is designed to take the id column as the _subject_, and set it to _type_  http://example.com/Employee. Then from each record, take a combination of the firstname and the lastname to be a _literal string object_ for the http://example.com/name  _predicate_. And finally, the occupation set to be an _“en-gb” literal object_.

### Example 2

This basic JSON example follows a similar structure to the CSV example, whereby the id column is taken to be the _subject._ 

### Example 3

This is basic CSV example designed for a performance test. It is a single subject graph with type of _Message_ identifid by the order number. The subject has three following properties: _hasMesage_ , _hasSha1_ and _hasSha256_ .

### Example 4

This is more complex CSV example designed for a performance test. The graph defined in the file contains three subjects of the following types: http://example.com/Employee (columns _ID_, _fistName_, _lastName_ ), http://example.com/Workplace ( _workplaceId_ , _workplaceName_ , _workplaceSuffix_ ) and workplace http://example.com/Address ( _addressId_ , _workplace_address_1stLane_ , _workplace_city_ , _workplace_postcode_ ).


For the performace tests purposes there is generated large example of this data (~2.4G). The file is located in S3 bucket: `s3://stream-lens/example4/input/staff-list.csv`.
