# Structured File Lens Examples

This repo contains examples of files used in the Structured File Lens. Each example will contain a sample input source file, the mapping file, as well as the output data and output provenance data.

### [Example 1](https://data-lens.atlassian.net/wiki/spaces/DLD/pages/305758400/Manually+Create+a+Mapping+File#CSV)

This basic CSV example is designed to take the id column as the _subject_, and set it to _type_  http://example.com/Employee. Then from each record, take a combination of the firstname and the lastname to be a _literal string object_ for the http://example.com/name  _predicate_. And finally, the occupation set to be an _“en-gb” literal object_.

### [Example 2](https://data-lens.atlassian.net/wiki/spaces/DLD/pages/305758400/Manually+Create+a+Mapping+File#JSON)

This basic JSON example follows a similar structure to the CSV example, whereby the id column is taken to be the _subject._ The ex:name  _object_ has not been assigned a termType, dataType, or language, so there for it must be an IRI. The structure of this JSON is a little more complicated than the CSV, the rml:iterator and the ex:occupation  _object_ both show how JSONPath is used in the mappings. By following the concatenation of the two, $.[*].employees.[*].role.occupation, we reach the occupation value for each employee.

### [Example 3](https://data-lens.atlassian.net/wiki/spaces/DLD/pages/305758400/Manually+Create+a+Mapping+File#XML)

This basic XML example also follows similar structures to the CSV and JSON example, taking id as _subject_, and the firstname and lastname as the ex:name  _predicate object._ However, the _subject_ value is an attribute of the xml, and the _object_ contains a user defined named graph, ex:EmployeesGraph, note the additional NQuads generated in the output as a result. Similarly to JSON, the XPath concatenation of the iterator and the reference in the template, /employees/employee/role/occupation, reaches the occupation value for each employee. And finally, as no parent nodes at being used in any of the XPath queries, we are using ql:SaXPath as the rml:referenceFormulation.

### [Example 4](https://data-lens.atlassian.net/wiki/spaces/DLD/pages/305758400/Manually+Create+a+Mapping+File#Full-Functions-Example)

This example shows a full mapping file containing functions within functions, along with an example input source CSV file and its expected RDF output.

### Example 5

This example shows a full mapping file, example input source CSV file and expected RDF output (including the provenance subgraph). Following the mapping file the data are grouped into three classes of entities: Employee, Department and Job which joins an employee with a department.  
