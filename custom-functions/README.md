# Custom Functions Examples

This repo contains examples of files used in the creation of custom functions. For more information and instructions on how to create your own functions, along with a description of the following example, see our [guide](https://data-lens.atlassian.net/wiki/spaces/DLD/pages/400392193/Create+Custom+Functions).